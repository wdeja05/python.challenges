class Animal:
    is_asleep = False

    def __init__(self, name, age, jump_factor):
        self.name = name
        self.age = age
        self.jump_factor = jump_factor

    def __str__(self) -> str:
        return f"{self.name} is a"

    def speak(self, sound="Wubba Lubba Dub Dub!"):
        return f"{self.name} speaks: {sound}"

    def jump(self, species="Animal"):
        jump_hight = self.jump_factor * .1
        return f"{self.name} jumps {jump_hight} m high."

    def get_sleep_status(self):
        if not self.is_asleep:
            return f"{self.name} is awake"
        else:
            return f"{self.name} is sleeping"
        pass

    def change_sleep_status(self):
            self.is_asleep = not self.is_asleep
            return self.is_asleep

class Chicken(Animal):
    species = 'Chicken'

    def __init__(self, name, age, weight, ):
        super().__init__(name, age, weight)
    
    def __str__(self) -> str:
        return f"{super().__str__()} {Chicken.species}"    
    
    def move_wings(self):
        return f"{self.name} is moving its wings!"

class Pig(Animal):
    species = 'Pig'

    def __init__(self, name, age, weight, sex):
        super().__init__(name, age, weight)
        self.sex = sex

    def __str__(self) -> str:
        return f"{super().__str__()} {pig.sex.lower()} {Pig.species}"
    
    def speak(self, sound):
        return super().speak(sound)
    

class Cow(Animal):
    species = 'Cow'

    def __init__(self, name, age, weight, color):
        super().__init__(name, age, weight)
        self.color = color
    
    def __str__(self) -> str:
        return f"{super().__str__()} {cow.color} {Cow.species}"
    

cow = Cow('Andy', 5, 87,'brown')
pig = Pig("Piglet", 3, 44, 'Female')
chicken = Chicken("Brandy", 1, .7)


print(pig)
print(cow)
print(chicken)

print(pig.speak("Oink Oink"))
print(cow.speak("Muu"))
print(cow.get_sleep_status())
cow.change_sleep_status()
print(cow.get_sleep_status())
print(chicken.move_wings())
