from enum import Enum

class Character():
    def __init__(self, name, hp, armor, atack, room="") -> None:
        self.name = name
        self.room = room
        self.hp = hp
        self.attack_points = atack
        self.armor = armor
        self.isDead = False

    def get_name(self) :
        return self.name

    def get_room(self) :
        return self.room

    def get_hp(self):
        return self.hp

    def set_hp(self, number):
        self.hp = number
        if self.hp < 0:
            self.die()
        return self.hp

    def get_attack_points(self):
        return self.attack_points

    def set_attack_points(self, number):
        self.attack_points += number
        return self.attack_points

    def get_armor(self):
        return self.armor

    def set_armor(self,number):
        self.armor += number
        return self.armor

    def get_is_dead(self):
        return self.isDead

    def die(self):
        self.isDead = True
        print (f"{self.get_name()} is dead")

class Hero(Character):
    def __init__(self, name, hp, armor, atack, room) -> None:
        super().__init__(name, hp, armor, atack, room)
        self.inventory = []

    def set_room(self, room):
        self.room = room
        return f"- Going to {self.room}"
    
    def find_in_inventory(self, search_item):
        if self.inventory == []:
            print("- Invenotry is empty")        
        else:
            for item in self.inventory:
                if search_item == item.get_name().lower():
                    return item

    def get_inventory(self):
        if self.inventory == []:
            print("- Invenotry is empty")
        else:
            for i in range(0,len(self.inventory)):
                print(f"{i+1}. {self.inventory[i].get_name()}")

    def add_inventory(self, item):
            self.inventory.append(item)
            return f"{item.get_name()} succesfully collected"

class Tool:
    def __init__(self, name,atack, armor, power="None", is_used=False) -> None:
        self.name = name
        self.is_used = is_used
        self.atack = atack
        self.armor = armor
        self.power = power

    def get_power(self):
        return self.power    
    
    def get_atack(self):
        return self.atack

    def get_armor(self):
        return self.armor

    def get_name(self):
        return self.name

    def use(self):
        self.is_used = True
        return self.is_used

    def get_is_used(self):
        return self.is_used

class Room:
    def __init__(self,name,description, tool_list = [], monster_list=[]) -> None:
        self.name = name
        self.description = description
        self.tool_list = tool_list
        self.monster_list = monster_list

    def get_name(self):
        return self.name
    
    def get_description(self):
        return self.description

    def get_tool_list(self):
        return self.tool_list

    def item_picekd(self):
        self.tool_list= []

    def get_monster(self):
        if len(self.monster_list) > 0:
            return self.monster_list[0]
        else:
            print("There are no mosters left")
            return 0


class FightManager:
    def atack(atacker,atacked):
        atack = atacker.get_attack_points()
        hp = atacked.get_hp()
        armor = atacked.get_armor()
        atack_power = (atack - armor)
        if atack_power < 0: 
            atack_power = 0
        result = hp - atack_power
        atacked.set_hp(result)
        return "Attack completed"

class Story(Enum):   
    LIST_ROOM ="""    - Spawn
    - Train Area
    - Forest

    - """

    WELCOME = """"
--------------------------------------------
      Yung Knight: the game
--------------------------------------------    
                Menu:
--------------------------------------------
- Start
- Exit
--------------------------------------------
"""

    BEGGINING = """
    Welcome!
--------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
    I wish you luck!!"""

    START = """
What's your next move?:
-"""

    WIN = """
        Congratulations!
--------------------------------------------------------------------------------------------------------------
Congratulations, player! 
--------------------------------------------------------------------------------------------------------------
        Well done, adventurer!"""

    LOOSE = """
--------------------------------------------------------------------------------------------------------------            
Unfortunately, your adventure has come to a tragic end. 
--------------------------------------------------------------------------------------------------------------
            """
    HELP = """Available commands:
describe - Describe currnt room
change - Open change room list
show - Show inventory
take - collect aviable tool form room
use - Use tool from inventory
atack - atack a monster
"""
class Menu:
    def __init__(self) -> None:
        self.exit = False


    def menu_intreface(self):
        while(not self.exit):
            command = input(Story.START.value).lower()
            self.aviable_commnads(command)

    def aviable_commnads(self, command):
        if command == "start":
            game.start_game()
        if command == "exit":
            self.exit = True

class Game:
    def __init__(self) -> None:
        self.win = False
        self.loose = False   

    def start_game(self):
        print(Story.BEGGINING.value)
        while not self.win and not self.loose:
            command = input(Story.START.value).lower()   
            self.aviable_commnads(command)

    def aviable_commnads(self, command):
        if command == "help":
            print(Story.HELP.value)
        if command == "describe":
            room = hero.get_room()
            tool = room.get_tool_list()
            print(room.get_description())
        if command == "change":
            room = input(Story.LIST_ROOM.value).lower()
            print(f"---\nchosen: {room}\n---")
            if not key.get_is_used():
                print("Cant get out without a key")
            else:
                if room == spawn.get_name().lower():
                    print("Going to Spawn")
                    hero.set_room(spawn)
                elif room == traning_room.get_name().lower():
                    print("Going to traning area")
                    hero.set_room(traning_room)
                elif room == forest.get_name().lower():
                    if not sword_wooden.get_is_used():
                        print("Cant get out without a weapon")
                    else:
                        print("Going to the forest")
                        hero.set_room(forest)
                else:
                    print("room didnt change")
        if command == "show":
            hero.get_inventory()
        if command == "take":
            room = hero.get_room()
            tool = room.get_tool_list()
            if not tool == []:
                hero.add_inventory(tool[0])
                print(f"Collected {tool[0].get_name()}")
                room.item_picekd()
            else:
                print("Room is empty")
        if command == "use":
            hero.get_inventory()
            item = input("    -Choose item to use:").lower()
            chosen_item = hero.find_in_inventory(item)
            chosen_item.use()
        if command == "atack":
            room = hero.get_room()
            monster = room.get_monster()
            if monster != 0 and not monster.get_is_dead():
                print(f"There is a mosnter: {monster.get_name()}")
                choice = input(f"Do you want to atack {monster.get_name()}? yes/no\n- ")
                if choice == "yes":
                    if sword_wooden.get_is_used():
                        monster.die()
                        if boss.get_is_dead():
                            print("BOSS IS DEAD")
                            print(Story.WIN.value)
                            self.win = True
                            menu.exit = True
                    else:
                        print("you need a sword to fight")
                elif choice == "no":
                    print("leaving in peace")
            else:
                print("No monsters here")
            
game = Game()
menu = Menu()
key = Tool("Key",0,0,"Can open what is locked")
sword_wooden = Tool("Sword",2,0)
sword_epic = Tool("Super Sword", 100, 2,"Give massive confidence boost")
slime = Character("Slime",1,.4,.4)
boss = Character("Dragonoid",10,3,2)
spawn = Room("Spawn","spawn area",[key])
traning_room = Room("Train Area", "Traning area for newbe knights", [sword_wooden],[slime])
forest = Room("Forest", "Forest near the town. Inhabited by small monsters.",[sword_epic],[boss])
hero = Hero("Czarek",5,0,1,spawn)


if __name__ == '__main__':
    print(Story.WELCOME.value)
    menu.menu_intreface()



