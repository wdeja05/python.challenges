from pathlib import Path
import csv

src = Path.home() /  "practice_files" / "scores.csv"
dst = Path.home() /  "practice_files" / "high_scores.csv"

score_name = []
names = []
result = []

with src.open(mode="r", encoding="utf-8") as file:
    text = file.read().split("\n")[1:-1]
    for row in text:
        row_values = row.split(",")
        score_name.append(row_values)

    for name,score in score_name:
        if not name in names:
            names.append(name)

    dic = dict.fromkeys(names,0)

    for name,score in score_name:
        if int(score) > dic.get(name):
            dic[name] = int(score)

for name,score in dic.items():
    result.append([name,score])

with dst.open(mode="w", encoding="utf-8",newline="") as file:
    writer = csv.writer(file)
    writer.writerow(["name","high_score"])
    for player_score in result:
        writer.writerow(player_score)

