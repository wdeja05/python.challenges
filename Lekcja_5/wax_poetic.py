import random

nouns = ["fossil", "horse", "aardvark", "judge", "chef", "mango", "extrovert", "gorilla"]
verbs = ["kicks", "jingles", "bounces", "slurps", "meows", "explodes", "curdles"]
adjectives = ["furry", "balding", "incredulous", "fragrant", "exuberant", "glistening"]
prepositions = ["against", "after", "into", "beneath", "upon", "for", "in", "like", "over", "within"]
adverbs = ["curiously", "extravagantly", "tantalizingly", "furiously", "sensuously"]

random.shuffle(nouns)
random.shuffle(verbs)
random.shuffle(adjectives)
random.shuffle(prepositions)
random.shuffle(adverbs)

noun1, noun2, noun3 = nouns[0:3]
verb1, verb2, verb3 =  verbs[0:3]
adj1, adj2, adj3 = adjectives[0:3]
prep1, prep2 = prepositions[0:2]
adverb1 = adverbs[0]

if adj1[0] in 'aeiou':
    article = 'An'
else:
    article = 'A'

print(f'{article} {adj1} {noun1}\n')
print(f'{article} {adj1} {noun1} {verb1} {prep1} the {adj2} {noun2}')
print(f'{adverb1}, the {noun1} {verb2}')
print(f'the {noun2} {verb3} {prep2} a {adj3} {noun3}')