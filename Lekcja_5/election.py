import random

first_candidate = 'Candidate A'
second_candidate = 'Candidate B'

region_a = .87
region_b = .65
region_c = .17



def eleciton_in_particural_region(region_prob):
    if random.random() < region_prob:
        return first_candidate
    else:
        return second_candidate

def simulate_election(region_1,region_2,region_3):
    region_prob = [region_1,region_2,region_3]
    votes_for_candidate_A = 0 
    for region in region_prob:
        if eleciton_in_particural_region(region) == first_candidate:
             votes_for_candidate_A = votes_for_candidate_A + 1
    if votes_for_candidate_A > 1:
        return first_candidate
    else:
        return second_candidate

winner_a = 0
winner_b = 0

for n in range(0,10_000):
    if simulate_election(region_a,region_b,region_c) == first_candidate:
        winner_a = winner_a + 1
    else:
        winner_b = winner_b + 1
        
print(str(winner_a/10_000*100)+"%")
