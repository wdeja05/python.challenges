list_of_enrollment_values = []
list_of_tuition_fees = []

universities = [
    ['California Institute of Technology', 2175, 37704],
    ['Harvard', 19627, 39849],
    ['Massachusetts Institute of Technology', 10566, 40732],
    ['Princeton', 7802, 37000],
    ['Rice', 5879, 35551],
    ['Stanford', 19535, 40569],
    ['Yale', 11701, 40500]
]

def enrollment_stats(list_of_lists):
    for name, number_of_students, tuition_fees in list_of_lists:
           list_of_enrollment_values.append(number_of_students)
           list_of_tuition_fees.append(tuition_fees)
    return (list_of_enrollment_values,list_of_tuition_fees)

def mean(list):
    return (sum(list)/len(list))

def median(list):
    list.sort()
    list_lenght = len(list)
    middle_index = list_lenght - list_lenght // 2
    if list_lenght % 2 == 0:
        return (list[middle_index-1] + list[middle_index]) / 2
    else:
        return list[middle_index-1]

        

enrolment_values, tuition_fees = enrollment_stats(universities)

total_number_of_students = sum(enrolment_values)
total_tuition = sum(tuition_fees)
mean_of_students = mean(enrolment_values)
median_of_students = median(enrolment_values)
mean_of_tuitions = mean(tuition_fees)
median_of_tuitions = median(tuition_fees)

print('******************************')
print(f'Total students: {total_number_of_students:,}')
print(f'Total tuition: $ {total_tuition:,}\n')
print(f'Student mean: {mean_of_students:,.2f}')
print(f'Student median: {median_of_students:,}\n')
print(f'Tuition mean: $ {mean_of_tuitions:,.2f}')
print(f'Tuition median: $ {median_of_tuitions:,}')
print('******************************')
