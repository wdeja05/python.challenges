import random

def coin_flip():
    if random.randint(0,1) == 0:
        return 'heads'
    else:
        return 'tails'
    
def flip_both():
    heads_tally = 0
    tails_tally = 0
    while heads_tally == 0 or tails_tally == 0:
        result = coin_flip()
        if result == 'heads':
            heads_tally = heads_tally + 1
        else:
            tails_tally = tails_tally + 1
    return heads_tally + tails_tally

flips_on_avreage = 0

for trial in range(10_000):
    flips_on_avreage = flips_on_avreage + flip_both()

print(int(flips_on_avreage / 10_000))

    
    
