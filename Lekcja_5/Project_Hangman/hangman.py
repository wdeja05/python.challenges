from getpass import getpass
secret_word = getpass('Enther secret word to sart the game: ').upper()
guessed_word = list('_' * len(secret_word))
max_error_number = 5
error_number = 0
list_of_used_letters = []

def draw_hangman(number):
    hangman = {
        0:"""
         
         




        / \\""",
        1:"""
         |
         |
         |
         |
         |
         |
        / \\""",
        2:"""
         |---------
         |
         |
         |
         |
         |
        / \\""",
        3:"""
         |---------
         |	 |
         |
         |
         |
         |
        / \\""",
        4:"""
         |---------
         |	 |
         |	 O
         |	/|\\
         |	/ \\
         |
        / \\""",
        5:"""
         |---------
         |	 |
         |	 0
         |	/|\\
         |	/ \\
         |
        / \\""",
    }
    print(hangman[number])
    

while(error_number < max_error_number):
    guessed_letter = input('Enter new letter: ').upper()
    is_in_secret_word = False

    for i in range(0,len(secret_word)):
        if secret_word[i] == guessed_letter:
           guessed_word[i] = guessed_letter
           print(' '.join(guessed_word))
           is_in_secret_word = True

    if not is_in_secret_word:
        error_number = error_number + 1
        print('Wrong letter!')
        draw_hangman(error_number)
        list_of_used_letters.append(guessed_letter)
        print(f'{error_number}/5')

    if list(secret_word) == guessed_word:
        print('*****************************\n')
        print("!!! CONGRATULATIOS - YOU HAVE WON !!!\n")
        print('*****************************')
        break
    
    string_of_used_letters = ' '.join(list_of_used_letters)
    print(f'Used letters: {string_of_used_letters}')
 
if error_number == 5:
    print('*****************************\n')
    print('!!! YOU HAVE LOST !!!\n')
    print('*****************************')
    