hats_with_cats = [False] * 100

def put_hats():
    for j in range(1,101):
        for n in range(1,101):
            if n%j == 0:
                if hats_with_cats[n-1] == True:
                    hats_with_cats[n-1] = False
                else:
                    hats_with_cats[n-1] = True

def print_cats():
    i=1
    for cat in hats_with_cats:
        if cat == True:
            print(f'Cat number {i} has a hat.')
        else:
            print(f'Cat number {i} doesn\'t have a hat.')
        i += 1

put_hats()      
print_cats()