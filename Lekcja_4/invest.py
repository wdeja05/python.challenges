def invest(amount, rate, years):
    for year in range(1,years+1):
        amount += amount*rate
        print(f'year {year}: {amount:.2f}')
    return 0

amount = float(input('Enter an amount you want to invest: '))
rate = float(input('Enter an investment rate: '))
years = int(input('Enter a number of years: '))
invest(amount, rate, years)
        
