def convert_cel_to_far(celcius_degree):
    fahrenheit_degree = celcius_degree * 9/5 + 32
    return fahrenheit_degree

def convert_far_to_cel(fahrenheit_degree):
    celcius_degree = (fahrenheit_degree - 32) * 5/9
    return celcius_degree

fahrenheit_degree = float(input('Enter a temperature in degrees F: '))
print(f'{fahrenheit_degree} degrees F = {convert_far_to_cel(fahrenheit_degree):.2f} degrees C\n')

celcius_degree = float(input('Enter a temperature in degrees C: '))
print(f'{celcius_degree} degrees F = {convert_cel_to_far(celcius_degree):.2f} degrees F')

