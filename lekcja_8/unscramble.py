from PyPDF2 import PdfFileReader
from PyPDF2 import PdfFileWriter
from pathlib import Path

# 1. Wcztaj plik pdf 
pdf_path = (Path.home() / "scrambled.pdf")
input_pdf = PdfFileReader(str(pdf_path))
writer = PdfFileWriter()
page_list = []

# 2. Pętla -> Lista obiketów stron
for n in range(0,input_pdf.getNumPages()):
    page = input_pdf.getPage(n)
    page_list.append(page)

# 3. Rotacja stron
for page in page_list:
    rotation = int(page["/Rotate"])
    while rotation != 0:
        if rotation < 0:
            page.rotateClockwise(90)
        else:
            page.rotateCounterClockwise(90)
        rotation = int(page["/Rotate"]) 

# 4. Segregacja stron
for n in range(0,len(page_list)):
    for page in page_list:
        page_number = int(page.extract_text())
        if page_number == n+1:
            writer.addPage(page)

# 5. zapis pliku pdf
output_path = (Path.home() / "unscrambled.pdf")
with Path(output_path).open(mode="wb") as output_file:
    writer.write(output_file)

