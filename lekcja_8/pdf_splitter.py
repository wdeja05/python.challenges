from PyPDF2 import PdfFileReader
from PyPDF2 import PdfFileWriter
from pathlib import Path

class PdfFileSplitter:
    def __init__(self,path) -> None:
        self.path = path
        self.input_pdf = PdfFileReader(str(self.path))
        self.number_of_pages = self.input_pdf.getNumPages()

    def split(self,breakpoint):
        self.writer1 = PdfFileWriter() # 0  to breakpoint
        self.writer2 = PdfFileWriter() # breakpoint to getNumPages()
        for n in range(0,self.number_of_pages):
            page = self.input_pdf.getPage(n)
            if n < breakpoint-1:
                self.writer1.addPage(page)
            else:
                self.writer2.addPage(page)

    def write(self,filename):
        output_path_1 = str(Path.home()/f"{filename}_1.pdf")
        output_path_2 = str(Path.home()/f"{filename}_2.pdf")
        with Path(output_path_1).open(mode="wb") as output_file:
            self.writer1.write(output_file)
        with Path(output_path_2).open(mode="wb") as output_file:
            self.writer2.write(output_file)


pdf_path = (Path.home() / "Pride_and_Prejudice.pdf")
splitter = PdfFileSplitter(pdf_path)
splitter.split(150)
splitter.write("pdf_split")

